# README

This code is a sample code to test Windows Event Object and Unicode support. 


## Prerequisites

The code assumes that: 

 a. You are using mingw-w64 cross compiler in Linux/Unix to cross compile the code for Windows 64-bit.

 b. You have cmake installed in your system.


## How to use

These are the steps to build and use the code:

 a. Invoke ./cross_build.sh to build the Windows 64-bit executable.

 b. The executable result will be in build-x86_64-w64-mingw32 directory.